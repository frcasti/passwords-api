# frozen_string_literal: true

module Api
  module V1
    class BaseController < ApplicationController
      include Pundit

      rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
      rescue_from ActionController::RoutingError, with: :route_not_found
      rescue_from ActionController::InvalidAuthenticityToken, with: :invalid_auth_token
      rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

      before_action :authenticate_user!

      respond_to :json

      private

      def route_not_found
        render json: 'Invalid route', status: :not_found
      end

      def record_not_found
        render json: nil, status: :not_found
      end

      def invalid_auth_token
        logger.debug { "NOT Authorized" } 
        respond_to do |format|
          format.html do
            redirect_to sign_in_path,
                        error: 'Login invalid or expired'
          end
          format.json { render json: 'invalid or expired token', status: :unauthorized }
        end
      end

      def user_not_authorized
        render json: {error: 'Not authorize'}, status: :unauthorized
      end
    end
  end
end
