class Api::V1::RolesController < Api::V1::BaseController
  before_action :authorize_admin
  before_action :set_role, only: %i[update destroy]

  def index
    @roles = Role.search( params[:search] || '' )

    render json: @roles, status: :ok
  end

  def create
    @role = Role.new(role_params)
    if @role.save
      render json: @role, status: :created
    else
      render json: @role.errors, status: :unprocessable_entity
    end
  end

  def update
    if @role.update(role_params)
      render json: @role
    else
      render json: @role.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @role.destroy
      render json: nil, status: :no_content
    else
      render json: @role.errors, status: :unprocessable_entity
    end
  end

  private

  def set_role
    @role = Role.find(params[:id])
  end

  def role_params
    params.require(:role).permit(:name)
  end

  def authorize_admin
    authorize Role
  end
end
