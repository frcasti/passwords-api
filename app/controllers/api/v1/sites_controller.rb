class Api::V1::SitesController < Api::V1::BaseController
  before_action :set_site, only: %i[update destroy]
  before_action :authorize_admin, only: %i[update destroy]

  def index
    @sites = Site.search( params[:search] || '' )

    render json: @sites, status: :ok
  end

  def create
    @site = Site.new(site_params)
    authorize @site
    if @site.save
      render json: @site, status: :created
    else
      render json: @site.errors, status: :unprocessable_entity
    end
  end

  def update
    if @site.update(site_params)
      render json: @site
    else
      render json: @site.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @site.destroy
      render json: nil, status: :no_content
    else
      render json: @site.errors, status: :unprocessable_entity
    end
  end

  private

  def set_site
    @site = Site.find(params[:id])
  end

  def site_params
    params.require(:site).permit(:name, :site_type, :agency, :url, :user, :password)
  end

  def authorize_admin
    authorize @site
  end
end
