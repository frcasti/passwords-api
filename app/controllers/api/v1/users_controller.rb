class Api::V1::UsersController < Api::V1::BaseController
  before_action :authorize_admin, except: %i[update]
  before_action :set_user, only: %i[update destroy]

  def index
    @users = User.search( params[:search] || '' )

    render json: @users, status: :ok
  end

  def create
    @user = User.new(user_params)
    if @user.save
      render json: @user, status: :created
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # def update
  #   if current_user.admin? || current_user == @user
  #     if @user.update(user_params)
  #       render json: @user
  #     else
  #       render json: @user.errors, status: :unprocessable_entity
  #     end
  #   else
  #     render json: {error: 'No admin user to update other users'}, status: :unprocessable_entity
  #   end
  # end

  def update
    authorize @user
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @user.destroy
      render json: nil, status: :no_content
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :role_id)
  end

  def authorize_admin
    authorize User
  end
end