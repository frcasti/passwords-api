class Role < ApplicationRecord
  has_many :users

  validates :name, presence: true, uniqueness: true

  def self.search search = ''
    if search && search != ''
      search = search.downcase
      self.where('lower(name) LIKE ?', "%#{search}%")
    else
      self.all
    end
  end
end
