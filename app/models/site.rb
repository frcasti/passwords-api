class Site < ApplicationRecord
  include Encryptable
  validates :name, :site_type, presence: true

  attr_encrypted :password

  def self.all_types
    self.all.pluck(:site_type).uniq
  end
  
  def self.all_agencies
    self.all.pluck(:agency).uniq
  end

  def self.search search = ''
    if search && search != ''
      search = search.downcase
      self.where("lower(name) LIKE ? or lower(site_type) LIKE ? or lower(agency) LIKE ?", "%#{search}%", "%#{search}%", "%#{search}%")
    else
      self.all
    end
  end

  def as_json(options = {})
    json = super
    options['password'] = password
    options['encrypted_password'] = nil
    json.merge(options)
  end
end
