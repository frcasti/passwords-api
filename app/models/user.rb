class User < ApplicationRecord
  include Devise::JWT::RevocationStrategies::JTIMatcher
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :recoverable, :rememberable, :validatable,
         :jwt_authenticatable, jwt_revocation_strategy: self

  belongs_to :role

  def admin?
    self.role.name == 'Administrator'
  end

  def self.search search = ''
    if search && search != ''
      search = search.downcase
      self.where('lower(email) LIKE ?', "%#{search}%")
    else
      self.all
    end
  end
end
