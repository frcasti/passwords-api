%w[
  PASS_KEY_BASE
  PASS_ENCRYPTION_SERVICE_SALT
  DEVISE_JWT_SECRET_KEY
].each do |env_var|
  if !ENV.has_key?(env_var) || ENV[env_var].blank?
    raise <<~EOL
    Missing environment variable: #{env_var}

    Ask a teammate for the appropriate value.
    EOL
  end
end