Warden::JWTAuth.configure do |config|
  config.secret = ENV['DEVISE_JWT_SECRET_KEY']
  config.dispatch_requests = [
                               ['POST', %r{^/api/v1/login$}],
                               ['POST', %r{^/api/v1/login.json$}],
                               ['POST', %r{^/api/v1/signup$}],
                               ['POST', %r{^/api/v1/signup.json$}],
											         ['GET', %r{^/api/v1/confirmation\?confirmation_token=.*$}],
											         ['GET', %r{^/api/v1/confirmation.json\?confirmation_token=.*$}]
                             ]
  config.revocation_requests = [
                                 ['DELETE', %r{^/api/v1/logout$}],
                                 ['DELETE', %r{^/api/v1/logout.json$}]
                               ]
end
