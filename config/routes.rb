Rails.application.routes.draw do
  ########################################## API namespace
  devise_for :users, skip: :all, defaults: { format: :json }
  
  namespace :api do
    namespace :v1 do
      devise_for :users, defaults: { format: :json },
                         class_name: 'User',
                         skip: %i[registrations
                                  sessions
                                  invitations
                                  confirmations
                                  passwords
                                  unlocks],
                         path: '',
                         path_names: { sign_in: 'login', sign_out: 'logout' }

      devise_scope :user do
        # get 'confirmation', to: 'devise/confirmations#show'
        # post 'confirmation', to: 'devise/confirmations#create'
        post 'login',  to:'devise/sessions#create'
        delete 'logout', to: 'devise/sessions#destroy'
        # get 'remember_email/:phone', to: 'devise/sessions#remember_email'
        # post 'signup', to: 'devise/registrations#create'
        # get 'email_validate', to: 'devise/registrations#email_validate'
        # get 'phone_validate', to: 'devise/registrations#phone_validate'
        # get 'users/:id', to: 'devise/registrations#edit'
        # put 'users/:id', to: 'devise/registrations#update'
        # post 'password_recover', to: 'devise/passwords#create'
        # put 'password_recover', to: 'devise/passwords#update'
        # post 'user_recover', to: 'devise/passwords#user_recover'
      end

      resources :sites, only: %i[index create update destroy]
      resources :roles, only: %i[index create update destroy]
      resources :users, only: %i[index create update destroy]

    end
  end
end