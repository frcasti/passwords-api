class CreateSites < ActiveRecord::Migration[6.0]
  def change
    create_table :sites, id: :uuid do |t|
      t.string :name
      t.string :site_type
      t.string :agency
      t.string :url
      t.string :user
      t.string :password

      t.timestamps
    end
  end
end
