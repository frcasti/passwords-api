class AddRoleToUsers < ActiveRecord::Migration[6.0]
  def change
  	role = Role.find_or_create_by({name: 'Administrator'})

    add_reference :users, :role, null: true, foreign_key: true, type: :uuid

    User.all.each { |u| u.update_columns(role_id: role.id)  }
  end
end
