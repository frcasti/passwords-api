class CustomDeviseFailure < Devise::FailureApp
  def respond
    if http_auth?
      http_auth
    elsif request.content_type == "application/json"
      self.status = 401
      self.content_type = "application/json"
      self.response_body = {success: false, error: i18n_message}.to_json
    else
      self.status = 401
      self.content_type = "application/json"
      self.response_body = {success: false, error: i18n_message}.to_json
      # redirect
    end
  end
end