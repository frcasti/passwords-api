# frozen_string_literal: true
# require 'rails_helper'
# require 'spec_helper'
# require 'devise'
#
require 'rails_helper'

RSpec.describe Api::V1::Devise::SessionsController, type: :api do
	describe 'POST #create' do
    it 'return a success login and Bearer token' do
      FactoryBot.create(:user, email: 'castillo.gomez82@gmail.com', 
                               password: 'password1234') unless User.find_by_email('castillo.gomez82@gmail.com')

      user_login_params = {
        user: {
          email: 'castillo.gomez82@gmail.com',
          password: 'password1234'
        }
      }

      post '/api/v1/login.json', user_login_params
      expect(last_response.status).to eq 201
    end

    it 'return a error login if error password' do
      user_login_params = {
        user: {
          email: 'castillo.gomez82@gmail.com',
          password: 'password1234345'
        }
      }

      post '/api/v1/login.json', user_login_params
      expect(last_response.status).to eq 401
    end

    it 'return a error login if error email' do
      user_login_params = {
        user: {
          email: 'castillo.gomez82pepep@gmail.com',
          password: 'password1234'
        }
      }

      post '/api/v1/login.json', user_login_params
      expect(last_response.status).to eq 401
    end
	end

  describe 'DELETE #destroy' do
    it 'return a success logout when user logged' do
      FactoryBot.create(:user, email: 'castillo.gomez82@gmail.com', 
                               password: 'password1234') unless User.find_by_email('castillo.gomez82@gmail.com')

      user_login_params = {
        user: {
          email: 'castillo.gomez82@gmail.com',
          password: 'password1234'
        }
      }

      post '/api/v1/login.json', user_login_params
      expect(last_response.status).to eq 201

      delete '/api/v1/logout'
      expect(last_response.status).to eq 200
    end

    # it 'return a error login if error password' do
    #   user_login_params = {
    #     user: {
    #       email: 'castillo.gomez82@gmail.com',
    #       password: 'password1234345'
    #     }
    #   }

    #   post '/api/v1/login.json', user_login_params
    #   expect(last_response.status).to eq 401
    # end

    # it 'return a error login if error email' do
    #   user_login_params = {
    #     user: {
    #       email: 'castillo.gomez82pepep@gmail.com',
    #       password: 'password1234'
    #     }
    #   }

    #   post '/api/v1/login.json', user_login_params
    #   expect(last_response.status).to eq 401
    # end
	end
end