require 'rails_helper'

RSpec.describe Api::V1::RolesController, type: :api do

  let(:valid_attributes) do
    {
      name: 'Administrator1'
    }
  end

  let(:valid_update_attributes) do
    {
      name: 'Administrator2'
    }
  end

  # login 
  let(:user) { prepare_user }
  let(:auth) { login(user) }

  let(:user_no_admin) { prepare_no_admin }
  let(:auth_no_admin) { login(user_no_admin) }

  describe 'GET /index' do
    before(:each) do
      @role1 = FactoryBot.create(:role, name: 'name1')
      @role2 = FactoryBot.create(:role, name: 'nombre')
      @role3 = FactoryBot.create(:role, name: 'otronomb')
      @role4 = FactoryBot.create(:role, name: 'num3')

      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
      header 'Authorization', auth
    end

    it 'returns all role lists' do
      get '/api/v1/roles.json'
      expect(last_response.status).to eq 200

      expect(json.size).to eq Role.all.count
    end

    it 'returns role lists by search in name' do
      get '/api/v1/roles.json', {search: 'nombre'}
      expect(last_response.status).to eq 200

      expect(json.size).to eq 1
      expect(json[0]['id']).to eq @role2.id
      expect(json[0]['name']).to eq 'nombre'
    end

    it 'returns role lists by search in name' do
      get '/api/v1/roles.json', {search: 'nomb'}
      expect(last_response.status).to eq 200

      expect(json.size).to eq 2
      expect(json.pluck('id')).to match_array([@role2.id, @role3.id])
    end
  end

  describe 'GET /index no ADMIN' do
    before(:each) do
      @role1 = FactoryBot.create(:role, name: 'name1')
      @role2 = FactoryBot.create(:role, name: 'nombre2')
      @role3 = FactoryBot.create(:role, name: 'otronomb1')
      @role4 = FactoryBot.create(:role, name: 'num3')

      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
      header 'Authorization', auth_no_admin
    end

    it 'returns all role lists ERROR' do
      get '/api/v1/roles.json'
      expect(last_response.status).to eq 401
    end

    it 'returns role lists by search in name ERROR' do
      get '/api/v1/roles.json', {search: 'nombre'}
      expect(last_response.status).to eq 401
    end
  end

  describe 'GET /index without login' do
    before(:each) do
      @role1 = FactoryBot.create(:role, name: 'name1')
      @role2 = FactoryBot.create(:role, name: 'nombre2')
      @role3 = FactoryBot.create(:role, name: 'otronomb1')
      @role4 = FactoryBot.create(:role, name: 'num3')

      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
    end

    it 'returns all role lists ERROR' do
      get '/api/v1/roles.json'
      expect(last_response.status).to eq 401
    end

    it 'returns role lists by search in name ERROR' do
      get '/api/v1/roles.json', {search: 'nombre'}
      expect(last_response.status).to eq 401
    end
  end

  describe 'POST /create' do
    before(:each) do
      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
      header 'Authorization', auth
    end

    it 'returns valid create role' do
      post '/api/v1/roles.json', {role: valid_attributes}.to_json
      
      expect(last_response.status).to eq 201
      expect(json['name']).to eq 'Administrator1'
    end

    it 'returns invalid create role' do
      valid_attributes[:name] = nil

      post '/api/v1/roles.json', {role: valid_attributes}.to_json
      expect(last_response.status).to eq 422
    end
  end

  describe 'POST /create no ADMIN' do
    before(:each) do
      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
      header 'Authorization', auth_no_admin
    end

    it 'returns valid create role' do
      post '/api/v1/roles.json', {role: valid_attributes}.to_json
      
      expect(last_response.status).to eq 401
    end

    it 'returns invalid create role' do
      valid_attributes[:name] = nil

      post '/api/v1/roles.json', {role: valid_attributes}.to_json
      expect(last_response.status).to eq 401
    end
  end

  describe 'POST /create without login' do
    it 'returns invalid request' do
      post '/api/v1/roles.json', {role: valid_attributes}.to_json
      expect(last_response.status).to eq 401
    end
  end

  describe 'PUT /update' do
    before(:each) do
      @role = FactoryBot.create(:role)

      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
      header 'Authorization', auth
    end

    it 'returns valid updated role' do
      put '/api/v1/roles/'+@role.id+'.json', {role: valid_update_attributes}.to_json
      expect(last_response.status).to eq 200
      expect(json['id']).to eq @role.id
      expect(json['name']).to eq valid_update_attributes[:name]
      @role.reload
      expect(@role.name).to eq valid_update_attributes[:name]
    end

    it 'returns invalid create role' do
      valid_update_attributes[:name] = nil

      put '/api/v1/roles/'+@role.id+'.json', {role: valid_update_attributes}.to_json
      expect(last_response.status).to eq 422
    end

    it 'returns 404 update not exist role' do
      put '/api/v1/roles/other_id.json', {role: valid_update_attributes}.to_json
      expect(last_response.status).to eq 404
    end
  end

  describe 'PUT /update no ADMIN' do
    before(:each) do
      @role = FactoryBot.create(:role)

      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
      header 'Authorization', auth_no_admin
    end

    it 'returns valid updated role' do
      put '/api/v1/roles/'+@role.id+'.json', {role: valid_update_attributes}.to_json
      expect(last_response.status).to eq 401
    end

    it 'returns invalid create role' do
      valid_update_attributes[:name] = nil

      put '/api/v1/roles/'+@role.id+'.json', {role: valid_update_attributes}.to_json
      expect(last_response.status).to eq 401
    end

    it 'returns 404 update not exist role' do
      put '/api/v1/roles/other_id.json', {role: valid_update_attributes}.to_json
      expect(last_response.status).to eq 401
    end
  end

  describe 'PUT /update without login' do
    before(:each) do
      @role = FactoryBot.create(:role)
    end

    it 'returns invalid request' do
      put '/api/v1/roles/'+@role.id+'.json', {role: valid_update_attributes}.to_json
      expect(last_response.status).to eq 401
    end
  end

  describe 'DELETE /delete' do
    before(:each) do
      @role = FactoryBot.create(:role)

      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
      header 'Authorization', auth
    end

    it 'returns valid updated role' do
      expect do
        delete '/api/v1/roles/'+@role.id+'.json'
      end.to change {
        Role.all.count
      }.by(-1)
    end

    it 'returns 404 update not exist role' do
      delete '/api/v1/roles/other_id.json'
      expect(last_response.status).to eq 404
    end
  end

  describe 'DELETE /delete no ADMIN' do
    before(:each) do
      @role = FactoryBot.create(:role)

      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
      header 'Authorization', auth_no_admin
    end

    it 'returns valid updated role' do
      delete '/api/v1/roles/'+@role.id+'.json'
      expect(last_response.status).to eq 401
    end

    it 'returns 404 update not exist role' do
      delete '/api/v1/roles/other_id.json'
      expect(last_response.status).to eq 401
    end
  end

  describe 'DELETE /delete without login' do
    before(:each) do
      @role = FactoryBot.create(:role)
    end

    it 'returns invalid request' do
      delete '/api/v1/roles/'+@role.id+'.json'
      expect(last_response.status).to eq 401
    end
  end

end
