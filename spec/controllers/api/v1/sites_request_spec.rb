require 'rails_helper'

RSpec.describe Api::V1::SitesController, type: :api do

  let(:valid_attributes) do
    {
      name: 'Site name',
      site_type: 'FTP',
      agency: 'ClickCode',
      url: 'https://google.es',
      user: 'user_ftp',
      password: 'password1234'
    }
  end

  let(:valid_update_attributes) do
    {
      name: 'New Site name',
      site_type: 'SFTP',
      agency: 'new ClickCode',
      password: 'new_password1234'
    }
  end

  # login 
  let(:user) { prepare_user }
  let(:auth) { login(user) }

  let(:user_no_admin) { prepare_no_admin }
  let(:auth_no_admin) { login(user_no_admin) }

  describe 'GET /index' do
    before(:each) do
      @site1 = FactoryBot.create(:site, name: 'name1',
                                        site_type: 'FTP',
                                        agency: 'Clickcode')
      @site2 = FactoryBot.create(:site, name: 'nombre2',
                                        site_type: 'sFTP',
                                        agency: 'Noho')
      @site21 = FactoryBot.create(:site, name: 'otronomb1',
                                        site_type: 'Plesk',
                                        agency: 'NohoNTT')
      @site3 = FactoryBot.create(:site, name: 'num3',
                                        site_type: 'wordpress',
                                        agency: 'Ntt')

      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
      header 'Authorization', auth
    end

    it 'returns all site lists' do
      get '/api/v1/sites.json'
      expect(last_response.status).to eq 200

      expect(json.size).to eq 4
    end

    it 'returns site lists by search in name, site_type, agency' do
      get '/api/v1/sites.json', {search: 'nombre'}
      expect(last_response.status).to eq 200

      expect(json.size).to eq 1
      expect(json[0]['id']).to eq @site2.id
      expect(json[0]['name']).to eq 'nombre2'
    end

    it 'returns site lists by search in name, site_type, agency' do
      get '/api/v1/sites.json', {search: 'ftp'}
      expect(last_response.status).to eq 200

      expect(json.size).to eq 2
      expect(json.pluck('id')).to match_array([@site2.id, @site1.id])
    end

    it 'returns site lists by search in name, site_type, agency' do
      get '/api/v1/sites.json', {search: 'ntt'}
      expect(last_response.status).to eq 200

      expect(json.size).to eq 2
      expect(json.pluck('id')).to match_array([@site3.id, @site21.id])
    end

    it 'returns site lists by search in name, site_type, agency' do
      get '/api/v1/sites.json', {search: 'num'}
      expect(last_response.status).to eq 200

      expect(json.size).to eq 1
      expect(json.pluck('id')).to match_array([@site3.id])
    end
  end

  describe 'GET /index no ADMIN' do
    before(:each) do
      @site1 = FactoryBot.create(:site, name: 'name1',
                                        site_type: 'FTP',
                                        agency: 'Clickcode')
      @site2 = FactoryBot.create(:site, name: 'nombre2',
                                        site_type: 'sFTP',
                                        agency: 'Noho')
      @site21 = FactoryBot.create(:site, name: 'otronomb1',
                                        site_type: 'Plesk',
                                        agency: 'NohoNTT')
      @site3 = FactoryBot.create(:site, name: 'num3',
                                        site_type: 'wordpress',
                                        agency: 'Ntt')

      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
      header 'Authorization', auth_no_admin
    end

    it 'returns all site lists' do
      get '/api/v1/sites.json'
      expect(last_response.status).to eq 200

      expect(json.size).to eq 4
    end

    it 'returns site lists by search in name, site_type, agency' do
      get '/api/v1/sites.json', {search: 'nombre'}
      expect(last_response.status).to eq 200

      expect(json.size).to eq 1
      expect(json[0]['id']).to eq @site2.id
      expect(json[0]['name']).to eq 'nombre2'
    end

    it 'returns site lists by search in name, site_type, agency' do
      get '/api/v1/sites.json', {search: 'ftp'}
      expect(last_response.status).to eq 200

      expect(json.size).to eq 2
      expect(json.pluck('id')).to match_array([@site2.id, @site1.id])
    end

    it 'returns site lists by search in name, site_type, agency' do
      get '/api/v1/sites.json', {search: 'ntt'}
      expect(last_response.status).to eq 200

      expect(json.size).to eq 2
      expect(json.pluck('id')).to match_array([@site3.id, @site21.id])
    end

    it 'returns site lists by search in name, site_type, agency' do
      get '/api/v1/sites.json', {search: 'num'}
      expect(last_response.status).to eq 200

      expect(json.size).to eq 1
      expect(json.pluck('id')).to match_array([@site3.id])
    end
  end

  describe 'GET /index without login' do
    before(:each) do
      @site1 = FactoryBot.create(:site, name: 'name1',
                                        site_type: 'FTP',
                                        agency: 'Clickcode')
      @site2 = FactoryBot.create(:site, name: 'nombre2',
                                        site_type: 'sFTP',
                                        agency: 'Noho')
      @site21 = FactoryBot.create(:site, name: 'otronomb1',
                                        site_type: 'Plesk',
                                        agency: 'NohoNTT')
      @site3 = FactoryBot.create(:site, name: 'num3',
                                        site_type: 'wordpress',
                                        agency: 'Ntt')

      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
    end

    it 'returns all site lists ERROR' do
      get '/api/v1/sites.json'
      expect(last_response.status).to eq 401
    end

    it 'returns site lists by search in name, site_type, agency ERROR' do
      get '/api/v1/sites.json', {search: 'nombre'}
      expect(last_response.status).to eq 401
    end

    it 'returns site lists by search in name, site_type, agency ERROR' do
      get '/api/v1/sites.json', {search: 'ftp'}
      expect(last_response.status).to eq 401
    end

    it 'returns site lists by search in name, site_type, agency ERROR' do
      get '/api/v1/sites.json', {search: 'ntt'}
      expect(last_response.status).to eq 401
    end

    it 'returns site lists by search in name, site_type, agency ERROR' do
      get '/api/v1/sites.json', {search: 'num'}
      expect(last_response.status).to eq 401
    end
  end

  describe 'POST /create' do
    before(:each) do
      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
      header 'Authorization', auth
    end

    it 'returns valid create site' do
      post '/api/v1/sites.json', {site: valid_attributes}.to_json
      
      expect(last_response.status).to eq 201
      expect(json['name']).to eq 'Site name'
      expect(json['site_type']).to eq 'FTP'
      expect(json['agency']).to eq 'ClickCode'
      expect(json['url']).to eq 'https://google.es'
      expect(json['user']).to eq 'user_ftp'
      expect(json['password']).to eq 'password1234'
    end

    it 'returns invalid create site' do
      valid_attributes[:name] = nil

      post '/api/v1/sites.json', {site: valid_attributes}.to_json
      expect(last_response.status).to eq 422
    end

    it 'returns invalid create site' do
      valid_attributes[:site_type] = nil

      post '/api/v1/sites.json', {site: valid_attributes}.to_json
      expect(last_response.status).to eq 422
    end
  end

  describe 'POST /create no ADMIN' do
    before(:each) do
      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
      header 'Authorization', auth_no_admin
    end

    it 'returns valid create site' do
      post '/api/v1/sites.json', {site: valid_attributes}.to_json
      expect(last_response.status).to eq 401
    end

    it 'returns invalid create site' do
      valid_attributes[:name] = nil

      post '/api/v1/sites.json', {site: valid_attributes}.to_json
      expect(last_response.status).to eq 401
    end

    it 'returns invalid create site' do
      valid_attributes[:site_type] = nil

      post '/api/v1/sites.json', {site: valid_attributes}.to_json
      expect(last_response.status).to eq 401
    end
  end

  describe 'POST /create without login' do
    it 'returns invalid request' do
      post '/api/v1/sites.json', {site: valid_attributes}.to_json
      expect(last_response.status).to eq 401
    end
  end

  describe 'PUT /update' do
    before(:each) do
      @site = FactoryBot.create(:site)

      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
      header 'Authorization', auth
    end

    it 'returns valid updated site' do
      put '/api/v1/sites/'+@site.id+'.json', {site: valid_update_attributes}.to_json
      expect(last_response.status).to eq 200
      expect(json['id']).to eq @site.id
      expect(json['name']).to eq valid_update_attributes[:name]
      expect(json['site_type']).to eq valid_update_attributes[:site_type]
      expect(json['agency']).to eq valid_update_attributes[:agency]
      expect(json['password']).to eq valid_update_attributes[:password]
      @site.reload
      expect(@site.name).to eq valid_update_attributes[:name]
      expect(@site.site_type).to eq valid_update_attributes[:site_type]
      expect(@site.agency).to eq valid_update_attributes[:agency]
      expect(@site.password).to eq valid_update_attributes[:password]
    end

    it 'returns invalid create site' do
      valid_update_attributes[:name] = nil

      put '/api/v1/sites/'+@site.id+'.json', {site: valid_update_attributes}.to_json
      expect(last_response.status).to eq 422
    end

    it 'returns invalid create site' do
      valid_update_attributes[:site_type] = nil

      put '/api/v1/sites/'+@site.id+'.json', {site: valid_update_attributes}.to_json
      expect(last_response.status).to eq 422
    end

    it 'returns 404 update not exist site' do
      put '/api/v1/sites/other_id.json', {site: valid_update_attributes}.to_json
      expect(last_response.status).to eq 404
    end
  end

  describe 'PUT /update no ADMIN' do
    before(:each) do
      @site = FactoryBot.create(:site)

      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
      header 'Authorization', auth_no_admin
    end

    it 'returns valid updated site' do
      put '/api/v1/sites/'+@site.id+'.json', {site: valid_update_attributes}.to_json
      expect(last_response.status).to eq 401
    end

    it 'returns invalid create site' do
      valid_update_attributes[:name] = nil

      put '/api/v1/sites/'+@site.id+'.json', {site: valid_update_attributes}.to_json
      expect(last_response.status).to eq 401
    end

    it 'returns invalid create site' do
      valid_update_attributes[:site_type] = nil

      put '/api/v1/sites/'+@site.id+'.json', {site: valid_update_attributes}.to_json
      expect(last_response.status).to eq 401
    end

    it 'returns 404 update not exist site' do
      put '/api/v1/sites/other_id.json', {site: valid_update_attributes}.to_json
      expect(last_response.status).to eq 404
    end
  end

  describe 'PUT /update without login' do
    before(:each) do
      @site = FactoryBot.create(:site)
    end

    it 'returns invalid request' do
      put '/api/v1/sites/'+@site.id+'.json', {site: valid_update_attributes}.to_json
      expect(last_response.status).to eq 401
    end
  end

  describe 'DELETE /delete' do
    before(:each) do
      @site = FactoryBot.create(:site)

      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
      header 'Authorization', auth
    end

    it 'returns valid updated site' do
      expect do
        delete '/api/v1/sites/'+@site.id+'.json'
      end.to change {
        Site.all.count
      }.by(-1)
    end

    it 'returns 404 update not exist site' do
      delete '/api/v1/sites/other_id.json'
      expect(last_response.status).to eq 404
    end
  end

  describe 'DELETE /delete no ADMIN' do
    before(:each) do
      @site = FactoryBot.create(:site)

      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
      header 'Authorization', auth_no_admin
    end

    it 'returns valid updated site' do
      delete '/api/v1/sites/'+@site.id+'.json'
      expect(last_response.status).to eq 401
    end

    it 'returns 404 update not exist site' do
      delete '/api/v1/sites/other_id.json'
      expect(last_response.status).to eq 404
    end
  end

  describe 'DELETE /delete without login' do
    before(:each) do
      @site = FactoryBot.create(:site)
    end

    it 'returns invalid request' do
      delete '/api/v1/sites/'+@site.id+'.json'
      expect(last_response.status).to eq 401
    end
  end

end
