require 'rails_helper'

RSpec.describe Api::V1::UsersController, type: :api do

  let(:valid_attributes) do
    {
      email: 'email1@email.com',
      password: 'password123456',
      password_confirmation: 'password123456',
      role_id: FactoryBot.create(:role).id
    }
  end

  let(:valid_update_attributes) do
    {
      email: 'email2@email.com',
      password: 'password123456',
      password_confirmation: 'password123456',
      role_id: FactoryBot.create(:role).id
    }
  end

  # login 
  let(:user) { prepare_user }
  let(:auth) { login(user) }

  let(:user_no_admin) { prepare_no_admin }
  let(:auth_no_admin) { login(user_no_admin) }

  describe "GET /index" do
    before(:each) do
      @user1 = FactoryBot.create(:user, email: 'em1@email.com')
      @user2 = FactoryBot.create(:user, email: 'em2@email.com')
      @user3 = FactoryBot.create(:user, email: 'em3@email.com')
      @user4 = FactoryBot.create(:user, email: 'em4@email.com')

      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
      header 'Authorization', auth
    end

    it "returns all users" do
      get '/api/v1/users.json'
      expect(last_response.status).to eq 200

      expect(json.size).to eq 5
      expect(json.pluck('email')).to include(@user1.email, @user2.email, @user3.email, @user4.email)
    end

    it "returns users list by search email" do
      get '/api/v1/users.json', {search: 'em3'}
      expect(last_response.status).to eq 200

      expect(json.size).to eq 1
      expect(json[0]['email']).to eq @user3.email
    end
  end

  describe "GET /index no ADMIN" do
    before(:each) do
      @user1 = FactoryBot.create(:user, email: 'em1@email.com')
      @user2 = FactoryBot.create(:user, email: 'em2@email.com')
      @user3 = FactoryBot.create(:user, email: 'em3@email.com')
      @user4 = FactoryBot.create(:user, email: 'em4@email.com')

      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
      header 'Authorization', auth_no_admin
    end

    it "returns all users" do
      get '/api/v1/users.json'
      expect(last_response.status).to eq 401
    end

    it "returns users list by search email" do
      get '/api/v1/users.json', {search: 'em3'}
      expect(last_response.status).to eq 401
    end
  end

  describe "GET /index whitout login" do
    before(:each) do
      @user1 = FactoryBot.create(:user, email: 'em1@email.com')
      @user2 = FactoryBot.create(:user, email: 'em2@email.com')
      @user3 = FactoryBot.create(:user, email: 'em3@email.com')
      @user4 = FactoryBot.create(:user, email: 'em4@email.com')

      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
    end

    it "returns all users" do
      get '/api/v1/users.json'
      expect(last_response.status).to eq 401
    end

    it "returns users list by search email" do
      get '/api/v1/users.json', {search: 'em3'}
      expect(last_response.status).to eq 401
    end
  end

  describe "POST /create" do
    before(:each) do
      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
      header 'Authorization', auth
    end

    it 'returns valid create user' do
      post '/api/v1/users.json', {user: valid_attributes}.to_json
      
      expect(last_response.status).to eq 201
      expect(json['email']).to eq valid_attributes[:email]
      expect(json['role_id']).to eq valid_attributes[:role_id]
    end

    it 'returns invalid create user' do
      valid_attributes[:email] = nil

      post '/api/v1/users.json', {user: valid_attributes}.to_json
      expect(last_response.status).to eq 422
    end
  end

  describe "POST /create no ADMIN" do
    before(:each) do
      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
      header 'Authorization', auth_no_admin
    end

    it 'returns valid create user' do
      post '/api/v1/users.json', {user: valid_attributes}.to_json

      expect(last_response.status).to eq 401
    end

    it 'returns invalid create user' do
      valid_attributes[:email] = nil

      post '/api/v1/users.json', {user: valid_attributes}.to_json
      expect(last_response.status).to eq 401
    end
  end

  describe "POST /create without login" do
    before(:each) do
      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
    end

    it 'returns valid create user' do
      post '/api/v1/users.json', {user: valid_attributes}.to_json

      expect(last_response.status).to eq 401
    end

    it 'returns invalid create user' do
      valid_attributes[:email] = nil

      post '/api/v1/users.json', {user: valid_attributes}.to_json
      expect(last_response.status).to eq 401
    end
  end

  describe "PUT /update" do
    before(:each) do
      @user = FactoryBot.create(:user)

      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
      header 'Authorization', auth
    end

    it 'returns valid update other user' do
      put '/api/v1/users/'+@user.id+'.json', {user: valid_update_attributes}.to_json

      expect(last_response.status).to eq 200
      expect(json['email']).to eq valid_update_attributes[:email]
      expect(json['role_id']).to eq valid_update_attributes[:role_id]
    end

    it 'returns valid update own user' do
      put '/api/v1/users/'+user.id+'.json', {user: valid_update_attributes}.to_json

      expect(last_response.status).to eq 200
      expect(json['email']).to eq valid_update_attributes[:email]
      expect(json['role_id']).to eq valid_update_attributes[:role_id]
    end

    it 'returns invalid update other user' do
      valid_update_attributes[:email] = nil

      put '/api/v1/users/'+@user.id+'.json', {user: valid_update_attributes}.to_json
      expect(last_response.status).to eq 422
    end

    it 'returns invalid update own user' do
      valid_update_attributes[:email] = nil

      put '/api/v1/users/'+user.id+'.json', {user: valid_update_attributes}.to_json
      expect(last_response.status).to eq 422
    end

    it 'returns not found update user' do
      put '/api/v1/users/other_id.json', {user: valid_update_attributes}.to_json
      expect(last_response.status).to eq 404
    end
  end

  describe "PUT /update no ADMIN" do
    before(:each) do
      @user = FactoryBot.create(:user)

      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
      header 'Authorization', auth_no_admin
    end

    it 'returns invalid update other user' do
      put '/api/v1/users/'+@user.id+'.json', {user: valid_update_attributes}.to_json
      expect(last_response.status).to eq 401
    end

    it 'returns valid update own user' do
      put '/api/v1/users/'+user_no_admin.id+'.json', {user: valid_update_attributes}.to_json
      expect(last_response.status).to eq 200
    end

    it 'returns invalid update other user' do
      valid_update_attributes[:email] = nil

      put '/api/v1/users/'+@user.id+'.json', {user: valid_update_attributes}.to_json
      expect(last_response.status).to eq 401
    end

    it 'returns invalid update own user' do
      valid_update_attributes[:email] = nil

      put '/api/v1/users/'+user_no_admin.id+'.json', {user: valid_update_attributes}.to_json
      expect(last_response.status).to eq 422
    end

    it 'returns not found update user' do
      put '/api/v1/users/other_id.json', {user: valid_update_attributes}.to_json
      expect(last_response.status).to eq 404
    end
  end

  describe "PUT /update without login" do
    before(:each) do
      @user = FactoryBot.create(:user)

      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
    end

    it 'returns valid update user' do
      put '/api/v1/users/'+@user.id+'.json', {user: valid_update_attributes}.to_json
      expect(last_response.status).to eq 401
    end

    it 'returns invalid update user' do
      valid_update_attributes[:email] = nil

      put '/api/v1/users/'+@user.id+'.json', {user: valid_update_attributes}.to_json
      expect(last_response.status).to eq 401
    end

    it 'returns not found update user' do
      put '/api/v1/users/other_id.json', {user: valid_update_attributes}.to_json
      expect(last_response.status).to eq 401
    end
  end

  describe 'DELETE /delete' do
    before(:each) do
      @user = FactoryBot.create(:user)

      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
      header 'Authorization', auth
    end

    it 'returns valid deleted user as admin' do
      expect do
        delete '/api/v1/users/'+@user.id+'.json'
      end.to change {
        User.all.count
      }.by(-1)
    end

    it 'returns 404 update not exist site' do
      delete '/api/v1/users/other_id.json'
      expect(last_response.status).to eq 404
    end
  end

  describe 'DELETE /delete no ADMIN' do
    before(:each) do
      @user = FactoryBot.create(:user)

      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
      header 'Authorization', auth_no_admin
    end

    it 'returns invalid delete other user' do
      delete '/api/v1/users/'+@user.id+'.json'
      expect(last_response.status).to eq 401
    end

    it 'returns invalid delete own user' do
      delete '/api/v1/users/'+user_no_admin.id+'.json'
      expect(last_response.status).to eq 401
    end

    it 'returns 404 update not exist site' do
      delete '/api/v1/users/other_id.json'
      expect(last_response.status).to eq 401
    end
  end

  describe 'DELETE /delete without login' do
    before(:each) do
      @user = FactoryBot.create(:user)

      header 'Content-Type', 'application/json'
      header 'Accept', 'application/json'
    end

    it 'returns invalid request' do
      delete '/api/v1/users/'+@user.id+'.json'
      expect(last_response.status).to eq 401
    end

    it 'returns 404 update not exist site' do
      delete '/api/v1/users/other_id.json'
      expect(last_response.status).to eq 401
    end
  end

end
