FactoryBot.define do
  factory :role do
    name { Faker::Alphanumeric.alpha(number: 6) }
  end
end
