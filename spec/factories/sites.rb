FactoryBot.define do
  factory :site do
    name { Faker::FunnyName.name }
    site_type { Faker::Types.rb_string}
    agency { Faker::Company.name }
    url { Faker::Internet.url }
    user { Faker::Internet.email }
    password { 'password1234' }
  end
end
