FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    password { 'password1234' }
    role { FactoryBot.create(:role) }
  end
end