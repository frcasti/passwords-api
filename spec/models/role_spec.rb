require 'rails_helper'

RSpec.describe Role, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_uniqueness_of(:name).ignoring_case_sensitivity }
  
  it { should respond_to(:users) }

  describe 'Roles' do
    before(:each) do
      @role = FactoryBot.create(:role, name: 'Administrator')
    end

    it 'not be valid' do
      role1 = Role.new
      expect(role1).to_not be_valid
    end

    it 'not be valid wit duplicate name' do
      role1 = Role.new(name: @role.name)
      expect(role1).to_not be_valid
    end

    it 'be valid with role' do
      role1 = Role.new(name: 'other_role')
      expect(role1).to be_valid
    end

    it 'be valid' do
      expect(@role).to be_valid
    end
  end
end
