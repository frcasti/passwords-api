require 'rails_helper'

RSpec.describe Site, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:site_type) }

  describe 'Scopes' do
    before(:each) do
      @site1 = FactoryBot.create(:site, site_type: 'FTP', agency: 'Noho')
      @site2 = FactoryBot.create(:site, site_type: 'SFTP', agency: 'NTT')
      @site3 = FactoryBot.create(:site, site_type: 'Wordpress', agency: 'ClickCode')
    end

    it 'return all site types' do
      site_types = Site.all_types
      expect(site_types).to match_array(['SFTP', 'Wordpress', 'FTP'])
    end

    it 'return all site agencies' do
      agencies = Site.all_agencies
      expect(agencies).to match_array(['ClickCode', 'Noho', 'NTT'])
    end

    it 'not return all site agencies' do
      agencies = Site.all_agencies
      expect(agencies).to_not match_array(['ClickCode', 'Noho', 'NTT', 'Other'])
    end

    it 'search sites by string syte_type' do
      sites = Site.search 'ftp'
      expect(sites).to match_array([@site1, @site2])
    end

    it 'search sites by string agency' do
      sites = Site.search 'click'
      expect(sites).to match_array([@site3])
    end
  end
end