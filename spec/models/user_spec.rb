require 'rails_helper'

RSpec.describe User, type: :model do
  it { should belong_to(:role) }

  it { should validate_uniqueness_of(:email).ignoring_case_sensitivity }

  describe 'Users' do
    before(:each) do
      @admin = FactoryBot.create(:user, email: 'admin@admin.com', role: Role.find_or_create_by(name: 'Administrator'))
      @user1 = FactoryBot.create(:user, email: 'admin_search@admin.com')
      @user2 = FactoryBot.create(:user, email: 'admin_other@admin.com')
    end

    it 'not be valid' do
      user1 = User.new
      expect(user1).to_not be_valid
    end

    it 'not be valid without role' do
      user1 = User.new(email: 'email@email.com', password: 'password123456')
      expect(user1).to_not be_valid
    end

    it 'be valid with role' do
      user1 = User.new(email: 'email@email.com', password: 'password123456', role: Role.find_or_create_by(name: 'Administrator'))
      expect(user1).to be_valid
    end

    it 'be valid' do
      expect(@admin).to be_valid
    end

    it 'not be valid with wrong email format' do
      @admin.email = 'test'
      expect(@admin).not_to be_valid
    end

    it 'not be valid with duplicate email' do
      user1 = User.new(email: 'admin@admin.com', password: 'password123456')
      expect(user1).to_not be_valid
    end

    it 'should return valid admin user' do
      expect(@admin.admin?).to be_truthy
    end

    it 'should return invalid admin user' do
      user1 = User.new(email: 'email@email.com', password: 'password123456', role: Role.find_or_create_by(name: 'Administrator2'))
      expect(user1.admin?).to_not be_truthy
    end

    it 'should return search users' do
      expect(User.search('admin').count).to eq 3
      expect(User.search('_searc').count).to eq 1
      expect(User.search('_other').count).to eq 1
    end
  end
end