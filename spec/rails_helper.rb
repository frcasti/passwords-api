ENV['RAILS_ENV'] ||= 'test'
require 'spec_helper'
require File.expand_path('../config/environment', __dir__)
require 'rspec/rails'

Dir[Rails.root.join("spec/support/**/*.rb")].each { |f| require f }
Dir[Rails.root.join("spec/models/concern/**/*.rb")].each { |f| require f }
Dir[Rails.root.join("spec/controllers/concern/**/*.rb")].each { |f| require f }

require 'database_cleaner'
require 'devise'
require 'simplecov'

# Run Simple Cov to show coverage test
SimpleCov.start

abort("The Rails environment is running in production mode!") if Rails.env.production?

DatabaseCleaner.strategy = :truncation


# configure shoulda matchers to use rspec as the test framework and full
# matcher libraries for rails
Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :rspec
    with.library :rails
  end
end

begin
  ActiveRecord::Migration.maintain_test_schema!
rescue ActiveRecord::PendingMigrationError => e
  puts e.to_s.strip
  exit 1
end
RSpec.configure do |config|
  # for Devise
  config.include Devise::Test::ControllerHelpers, type: :controller
  config.fixture_path = "#{::Rails.root}/spec/fixtures"
  # Load helper for login
  config.include Helpers::Authentication
  # Add FactoryBot methods
  config.include FactoryBot::Syntax::Methods

  # Filter lines from Rails gems in backtraces.
  config.filter_rails_from_backtrace!

  # Clean database and active storage assets
  DatabaseCleaner.strategy = :truncation
  config.after do
    DatabaseCleaner.clean
    FileUtils.rm_rf("#{Rails.root}/tmp/storage_test")
    FileUtils.rm_rf(Dir["#{Rails.root}/public/uploads/test/*"])
  end
end