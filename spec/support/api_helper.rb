module ApiHelper
  include Rack::Test::Methods

  def app
    Rails.application
  end

  # def prepare_image
  #   file = File.open(Rails.root.join('spec', 'support', 'assets', 'logo.jpg')).read
  #   "data:image/jpeg;base64,#{Base64.encode64(file)}"
  # end

  def prepare_user
    role = Role.find_or_create_by(name: 'Administrator')
    FactoryBot.create(:user, email: 'custom_email1@gmail.com', role: role)
  end
  
  def prepare_other_user
    FactoryBot.create(:user, email: 'custom_email2@gmail.com')
  end

  def prepare_no_admin
    role = FactoryBot.create(:role, name: 'no_admin')
    FactoryBot.create(:user, email: 'custom_email3@gmail.com', role: role)
  end

  # def prepare_user_admin
  # end

  def user_params(u)
    { user: { email: u.email, password: u.password } }
  end

  def login(user = nil)
    _user = user || prepare_user
    post('/api/v1/login.json', user_params(_user).to_json, format: :json)
    ApplicationController.send(:alias_method, :current_user, :current_user)
    ApplicationController.send(:define_method, :current_user) do
      _user
    end
    last_response.headers['Authorization']
  end
end