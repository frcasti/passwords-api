module Helpers
  module Authentication
    def login_an_user
      request.env["devise.mapping"] = Devise.mappings[:user]
      # Create user
      @user = FactoryBot.create(:user)
      # Confirm user
      @user.confirm
      # Sign in with this user
      authorized = sign_in(@user, scope: '/api/v1/login')
      # Find Autentication token
      headers = {'Accept' => 'application/json', 'Content-Type' => 'application/json'}
      auth = Devise::JWT::TestHelpers.auth_headers(headers, authorized)['Authorization']
      # Set Authorization headers for this user
      request.headers['Authorization'] = auth
      request.headers['Accept'] = 'application/json'
      request.headers['Content-Type'] = 'application/json'
    end
  end
end